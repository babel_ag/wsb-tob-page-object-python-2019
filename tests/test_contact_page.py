"""
Test methods for ContactPage.
"""

from pages.contact_page import ContactPage
from tests.contact_page_test_template import TestTemplate


class TestContactPage(TestTemplate):

    def test_title(self):
        """
        Test is checking if title of page is correct.
        :return: "Contact"
        """
        k = ContactPage(self.driver)
        assert k.check_title() == "Contact"

    def test_page_is_scrollable(self):
        """
        Test is checking that page is scrollable.
        :return: True
        """
        k = ContactPage(self.driver)
        k.scroll_down()
        assert k.is_footer_enabled() == True

    def test_logo_available(self):
        """
        Test is checking if logo is displayed at the main site.
        :return: True
        """
        k = ContactPage(self.driver)
        assert k.is_logo_enabled() == True

    def test_send_the_form(self):
        """
        Test will be checking send message to company.
        :return: "Message sent. Thank you for contacting ADVA,none of our team will be in touch with you shortly. Close"
        """
        k = ContactPage(self.driver)
        k.click_department_form_field()
        k.take_screenshot()
        k.click_event_dropdown_option()
        k.take_screenshot()
        k.clear_first_name_form_field()
        k.send_first_name_form_field()
        k.take_screenshot()
        k.clear_last_name_form_field()
        k.send_last_name_form_field()
        k.take_screenshot()
        k.clear_company_form_field()
        k.send_company_form_field()
        k.take_screenshot()
        k.click_country_dropdown_list()
        k.take_screenshot()
        k.click_poland_dropdown_option()
        k.take_screenshot()
        k.clear_email_form_field()
        k.send_email_form_field()
        k.take_screenshot()
        k.clear_telephone_form_field()
        k.send_telephone_form_field()
        k.take_screenshot()
        k.clear_comment_form_field()
        k.send_comment_form_field()
        k.take_screenshot()
        k.click_sumbit_button()
        k.take_screenshot()
        assert k.is_sent_message_enabled() == "Message sent\nThank you for contacting ADVA, " \
                                              "one of our team will be in touch with you shortly.\nClose"
        k.take_screenshot()


    def test_search_regional_office(self):
        """
        Test will be checking search regional detail office
        :return: "+39 06 8676 1027"
        """
        k = ContactPage(self.driver)
        k.scroll_down()
        assert k.is_logo_enabled() == True
        k.click_location_dropdown_list()
        k.click_italy_dropdown_option()
        assert k.find_office_number() == "+39 06 8676 1027"
