"""
Test methods for MainPage.
"""

from pages.main_page import MainPage
from tests.main_page_test_template import TestTemplate


class TestMainPage(TestTemplate):

    def test_title(self):
        """
        Test is checking if title of page is correct..
        :return: "ADVA"
        """
        m = MainPage(self.driver)
        assert m.check_title() == "ADVA"

    def test_cookie_banner(self):
        """
        Test is checking that cookie banner after clicked is not disable.
        :return: True
        """
        m = MainPage(self.driver)
        m.click_accept_button()
        assert m.is_cookie_banner_displayed() == True

    def test_search_engine(self):
        """
        After clicking on the button open search, we will be input "test" word into search engine to checking that
        search engine is working correctly. Finally we should be on the search results card.
        :return: "Search Results"
        """
        m = MainPage(self.driver)
        m.click_button_open_search()
        m.click_engine_placeholder()
        m.clear_engine_placeholder()
        m.send_text_to_engine_placeholder()
        m.click_submit_search_button()
        assert m.check_title() == "Search Results"

    def test_innovation_menu(self):
        """
        After mouse hovering innovation button menu should be visible.
        :return: True
        """
        m = MainPage(self.driver)
        m.find_innovation_menu()
        m.mouse_hover_innovation_menu()
        assert m.is_menu_innovation_list_enabled() == True

    def test_page_is_scrollable(self):
        """
        Test is checking that page is scrollable.
        :return: True
        """
        m = MainPage(self.driver)
        m.scroll_down()
        assert m.is_footer_enabled() == True

    def test_newsroom_menu(self):
        """
        After mouse hovering newsroom button menu should be visible.
        :return: True
        """
        m = MainPage(self.driver)
        m.find_newsroom_menu()
        m.mouse_hover_newsroom_menu()
        assert m.is_newsroom_menu_list_enabled() == True

    def test_logo_available(self):
        """
        Test is checking if logo is displayed at the main site..
        :return: True
        """
        m = MainPage(self.driver)
        assert m.is_logo_enabled() == True

    def test_product_menu(self):
        """
        After mouse hovering product button menu should be visible.
        :return: True
        """
        m = MainPage(self.driver)
        m.find_products_menu()
        m.mouse_hover_products_menu()
        assert m.is_products_menu_list_enabled() == True

    def test_open_search_button(self):
        """
        Test will be checking that open search button is enabled on the site.
        :return: True
        """
        m = MainPage(self.driver)
        assert m.is_button_open_search_enabled() == True

    def test_aboutus_menu(self):
        """
        After mouse hovering product button menu should be visible.
        :return: True
        """
        m = MainPage(self.driver)
        m.find_aboutus_menu()
        m.mouse_hover_aboutus_menu()
        assert m.is_aboutus_menu_list_enabled() == True
