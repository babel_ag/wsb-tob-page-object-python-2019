"""
Test methods for CareerPage.
"""

from pages.career_page import CareerPage
from tests.career_page_test_template import TestTemplate


class TestCareerPage(TestTemplate):

    def test_title(self):
        """
        Test is checking if title of page is correct.
        :return: "Careers"
        """
        c = CareerPage(self.driver)
        assert c.check_title() == "Careers"

    def test_logo_available(self):
        """
        Test is checking if logo is displayed at the main site.
        :return: True
        """
        c = CareerPage(self.driver)
        assert c.is_logo_enabled() == True

    def test_page_is_scrollable(self):
        """
        Test is checking that page is scrollable.
        :return: True
        """
        c = CareerPage(self.driver)
        c.scroll_down()
        assert c.is_footer_enabled() == True

    def test_switch_to_youtube(self):
        """
        Test will be checking switch to youtube site.
        :return: True
        """
        c = CareerPage(self.driver)
        c.scroll_down()
        c.click_youtube_button()
        assert c.is_youtube_button_enabled() == True

    def test_search_job_opportunities_filters(self):
        """
        Test will be checking filter job opportunities
        :return: "English"
        """
        c = CareerPage(self.driver)
        c.click_job_opportunities_button()
        c.click_all_departments_button()
        assert c.find_research_and_development_label_text() == "Research and Development"
        c.click_research_and_development_label()
        c.click_all_countries_button()
        assert c.find_usa_label_text() == "USA"
        c.click_usa_label()
        c.click_all_languages_button()
        assert c.is_english_label() == "English"
