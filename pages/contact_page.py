"""
All methods and locators which will use to build tests for ContactPage.
"""

from pages.base_page import BasePage
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import time


class ContactPage(BasePage):
    # Locators test_cookie_banner
    _logo = "//div[@id='logo']//img[@alt='ADVA Optical Networking']"

    # Locators test_pagecontact_is_scrollable
    _imprint_footer = "//footer[@id='site-footer']//a[@href='/en/about-us/legal/imprint']"

    # Locators test_sending_the_form
    _department_dropdown_list = "//div[3]//span[@class='text']"
    _event_dropdown_option = "//div[3]//div[@role='listbox']/div/div[3]"
    _first_name_form_field = "/html//input[@id='first-name']"
    _last_name_form_field = "/html//input[@id='last-name']"
    _company_form_field = "/html//input[@id='company-name']"
    _country_dropdown_list = "//div[7]//div[@class='stylish-select']/span"
    _poland_dropdown_option = "//div[@role='listbox']/div/div[178]"
    _email_form_field = "/html//input[@id='email-name']"
    _telephone_form_field = "/html//input[@id='tel-number']"
    _comment_form_field = "/html//textarea[@id='comments']"
    _sumbit_button = "//div[@id='main']/form//button[@type='submit']"
    _sent_message = "//body/div[3]/div[@class='mod-lightbox']"


    # Locators test_search_regional_office
    _location_dropdown_list = "//div[@id='main']/section[2]//div[@class='stylish-select']/span"
    _italy_dropdown_option = "//div[@id='main']/section[2]/div/section//div[@role='listbox']/div/div[10]"
    _office_number = "//a[@href='tel:+39 06 8676 1027']"

    # Text to send contact message
    FIRST_NAME = "Czesław"
    LAST_NAME = "Kowalski"
    COMPANY_NAME = "Lotos"
    E_MAIL = "czesiu@o2.pl"
    PHONE_NUMBER = "203203020"
    TEST_MESSAGE = "It is testing message"


    def take_screenshot(self):
        name = time.time_ns()
        return self._driver.save_screenshot(f"../screenshots_contact_field_form/{name}.png")

    def is_footer_enabled(self):
        return self._driver.find_element_by_xpath(self._imprint_footer).is_enabled()

    def is_logo_enabled(self):
        return self._driver.find_element_by_xpath(self._logo).is_enabled()

    def click_department_form_field(self):
        return self._driver.find_element_by_xpath(self._department_dropdown_list).click()

    def click_event_dropdown_option(self):
        return self._driver.find_element_by_xpath(self._event_dropdown_option).click()

    def clear_first_name_form_field(self):
        return self._driver.find_element_by_xpath(self._first_name_form_field).clear

    def send_first_name_form_field(self):
        return self._driver.find_element_by_xpath(self._first_name_form_field).send_keys(self.FIRST_NAME)

    def clear_last_name_form_field(self):
        return self._driver.find_element_by_xpath(self._last_name_form_field).clear

    def send_last_name_form_field(self):
        return self._driver.find_element_by_xpath(self._last_name_form_field).send_keys(self.LAST_NAME)

    def clear_company_form_field(self):
        return self._driver.find_element_by_xpath(self._company_form_field).clear

    def send_company_form_field(self):
        return self._driver.find_element_by_xpath(self._company_form_field).send_keys(self.COMPANY_NAME)

    def click_country_dropdown_list(self):
        return self._driver.find_element_by_xpath(self._country_dropdown_list).click()

    def click_poland_dropdown_option(self):
        return self._driver.find_element_by_xpath(self._poland_dropdown_option).click()

    def clear_email_form_field(self):
        return self._driver.find_element_by_xpath(self._email_form_field).clear

    def send_email_form_field(self):
        return self._driver.find_element_by_xpath(self._email_form_field).send_keys(self.E_MAIL)

    def clear_telephone_form_field(self):
        return self._driver.find_element_by_xpath(self._telephone_form_field).clear

    def send_telephone_form_field(self):
        return self._driver.find_element_by_xpath(self._telephone_form_field).send_keys(self.PHONE_NUMBER)

    def clear_comment_form_field(self):
        return self._driver.find_element_by_xpath(self._comment_form_field).clear

    def send_comment_form_field(self):
        return self._driver.find_element_by_xpath(self._comment_form_field).send_keys(self.TEST_MESSAGE)

    def click_sumbit_button(self):
        return self._driver.find_element_by_xpath(self._sumbit_button).click()

    def is_sent_message_enabled(self):
        wait = WebDriverWait(self._driver, 2)
        return wait.until(EC.presence_of_element_located((By.XPATH, self._sent_message))).text

    def click_location_dropdown_list(self):
        return self._driver.find_element_by_xpath(self._location_dropdown_list).click()

    def click_italy_dropdown_option(self):
        return self._driver.find_element_by_xpath(self._italy_dropdown_option).click()

    def find_office_number(self):
        return self._driver.find_element_by_xpath(self._office_number).text
