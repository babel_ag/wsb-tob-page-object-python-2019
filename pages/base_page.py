"""
BasePage class initialized driver and actions.
"""
import time

class BasePage(object):

    def __init__(self, driver):
        self._driver = driver

    def check_title(self):
        time.sleep(1)
        return self._driver.title

    def scroll_down(self):
        return self._driver.execute_script("window.scrollTo(0, 4000);")