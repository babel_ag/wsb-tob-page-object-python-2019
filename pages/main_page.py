"""
All methods and locators which will use to build tests for MainPage.
"""

from pages.base_page import BasePage
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains


class MainPage(BasePage):
    # Locators test_cookie_banner
    _accept_button = "//body/section//button[1]"
    _cookie_banner = "//body/section/div[@class='wrapper-2000']"

    # Locators test_cookie_banner
    _logo = "//img[@alt='ADVA Optical Networking']"

    # Locators test_page_is_scrollable
    _original_footer = "//li[.='© 2019 ADVA Optical Networking']"

    # Locators test_button_open_search
    _open_search_button = "//div[@class='search-container']/button[1]"

    # Loacators test_search_engine:
    _search_engine_placeholder = "//input[@name='search']"
    _submit_search_button = "submit-search"
    _search_value = "Test"

    # Locators test_product_menu
    _products_menu = "//div[@class='first-level-link']/a[@href='/en/products']"
    _products_menu_list = "//li[1]/div[@class='mega-nav']"

    # Locators test_innovation_menu
    _innovation_menu = "//div[@class='first-level-link']/a[@href='/en/innovation']"
    _innovation_menu_list = "//li[2]/div[@class='mega-nav']"

    # Locators test_newsroom_menu
    _newsroom_menu = "//div[@class='first-level-link']/a[@href='/en/newsroom']"
    _newsroom_menu_list = "//li[3]/div[@class='mega-nav']"

    # Locators test_aboutus_menu
    _aboutus_menu = "//div[@class='first-level-link']/a[@href='/en/about-us']"
    _aboutus_menu_list = "//li[5]/div[@class='mega-nav']"

    #Text to send
    TEXT_SEND_SEARCH_ENGINE = "Test"


    def click_accept_button(self):
        return self._driver.find_element_by_xpath(self._accept_button).click()

    def is_cookie_banner_displayed(self):
        return self._driver.find_element_by_xpath(self._cookie_banner).is_displayed()

    def is_logo_enabled(self):
        return self._driver.find_element_by_xpath(self._logo).is_enabled()

    def is_footer_enabled(self):
        return self._driver.find_element_by_xpath(self._original_footer).is_enabled()

    def is_button_open_search_enabled(self):
        return self._driver.find_element_by_xpath(self._open_search_button).is_enabled()

    def click_button_open_search(self):
        return self._driver.find_element_by_xpath(self._open_search_button).click()

    def click_engine_placeholder(self):
        return self._driver.find_element_by_xpath(self._search_engine_placeholder).click()

    def clear_engine_placeholder(self):
        return self._driver.find_element_by_xpath(self._search_engine_placeholder).clear

    def send_text_to_engine_placeholder(self):
        return self._driver.find_element_by_xpath(self._search_engine_placeholder).send_keys(self.TEXT_SEND_SEARCH_ENGINE)

    def click_submit_search_button(self):
        wait = WebDriverWait(self._driver, 5)
        return wait.until(EC.element_to_be_clickable((By.CLASS_NAME, self._submit_search_button))).click()

    def find_innovation_menu(self):
        return self._driver.find_element_by_xpath(self._innovation_menu).is_enabled()

    def mouse_hover_innovation_menu(self):
        actions = ActionChains(self._driver)
        return actions.move_to_element(self._driver.find_element_by_xpath(self._innovation_menu)).perform()

    def is_menu_innovation_list_enabled(self):
        return self._driver.find_element_by_xpath(self._innovation_menu_list).is_enabled()

    def find_newsroom_menu(self):
        return self._driver.find_element_by_xpath(self._newsroom_menu)

    def mouse_hover_newsroom_menu(self):
        actions = ActionChains(self._driver)
        return actions.move_to_element(self._driver.find_element_by_xpath(self._newsroom_menu)).perform()

    def is_newsroom_menu_list_enabled(self):
        return self._driver.find_element_by_xpath(self._newsroom_menu_list).is_enabled()

    def find_products_menu(self):
        return self._driver.find_element_by_xpath(self._products_menu)

    def mouse_hover_products_menu(self):
        actions = ActionChains(self._driver)
        return actions.move_to_element(self._driver.find_element_by_xpath(self._products_menu)).perform()

    def is_products_menu_list_enabled(self):
        return self._driver.find_element_by_xpath(self._products_menu_list).is_enabled()

    def find_aboutus_menu(self):
        return self._driver.find_element_by_xpath(self._aboutus_menu)

    def mouse_hover_aboutus_menu(self):
        actions = ActionChains(self._driver)
        return actions.move_to_element(self._driver.find_element_by_xpath(self._aboutus_menu)).perform()

    def is_aboutus_menu_list_enabled(self):
        return self._driver.find_element_by_xpath(self._aboutus_menu_list).is_enabled()
