"""
All methods and locators which will use to build tests for CareerPage.
"""

from pages.base_page import BasePage
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class CareerPage(BasePage):
    # Locators test_cookie_banner
    _logo = "//img[@alt='ADVA Optical Networking']"

    # Locators test_page__is_scrollable
    _sitemap_footer = "//footer[@id='site-footer']//li[1]/a[2]"

    # Locators test_switch_to_youtube
    _youtube_button = "//img[@alt='youtube']"
    _youtube_logo = "/html//div[@id='logo-icon-container']"

    # Locators test_search_job_opportunities
    _job_opportunities_button = "//section[2]//a[@href='/en/about-us/careers/job-opportunities']"
    _all_departments_button = "//div[1]/div[@class='stylish-select']//span[@class='text']"
    _research_and_development_label_text ="//div[@id='main']/section[2]/div/section[2]/div/div[1]/div/div[@role='listbox']/div/div[38]"
    _research_and_development_label = "//div[@id='main']/section[2]/div/section[2]/div/div[1]/div/div[@role='listbox']/div/div[38]"
    _all_countries_button = "//div[2]/div[@class='stylish-select']"
    _usa_label = "//div[@id='main']/section[2]/div/section[2]/div/div[2]/div/div[@role='listbox']/div/div[16]"
    _usa_label_text = "//div[@id='main']/section[2]/div/section[2]/div/div[2]/div/div[@role='listbox']/div/div[16]"
    _all_languages_button = "//div[3]/div[@class='stylish-select']"
    _english_label = "//div[3]/div/div[@role='listbox']/div/div[2]"


    def is_logo_enabled(self):
        return self._driver.find_element_by_xpath(self._logo).is_enabled()

    def is_footer_enabled(self):
        return self._driver.find_element_by_xpath(self._sitemap_footer).is_enabled()

    def click_youtube_button(self):
        return self._driver.find_element_by_xpath(self._youtube_button).click()

    def is_youtube_button_enabled(self):
        return self._driver.find_element_by_xpath(self._youtube_logo).is_enabled()

    def click_job_opportunities_button(self):
        return self._driver.find_element_by_xpath(self._job_opportunities_button).click()

    def click_all_departments_button(self):
        return self._driver.find_element_by_xpath(self._all_departments_button).click()

    def click_research_and_development_label(self):
        return self._driver.find_element_by_xpath(self._research_and_development_label).click()

    def find_research_and_development_label_text(self):
        return self._driver.find_element_by_xpath(self._research_and_development_label_text).text

    def click_all_countries_button(self):
        return self._driver.find_element_by_xpath(self._all_countries_button).click()

    def click_usa_label(self):
        return self._driver.find_element_by_xpath(self._usa_label).click()

    def find_usa_label_text(self):
        return self._driver.find_element_by_xpath(self._usa_label_text).text

    def click_all_languages_button(self):
        return self._driver.find_element_by_xpath(self._all_languages_button).click()

    def is_english_label(self):
        wait = WebDriverWait(self._driver, 10)
        return wait.until(EC.presence_of_element_located((By. XPATH, self._english_label))).text
